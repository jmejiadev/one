<?php
$target_dir = "";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
//if (file_exists($target_file)) {
//    echo "Sorry, file already exists.";
//    $uploadOk = 0;
//}
// Check file size

if ($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "Tamaño de archivo no soportado";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "csv" && $imageFileType != "txt" ) {
    echo "Extensión de archivo no compatible";
    $uploadOk = 0;
}


    $usuario = $_POST['usuario'];
    $contrasena = $_POST['contrasena'];
    $servidor = $_POST['servidor'];
    $basededatos = $_POST['basededatos'];
    $conexion = mysqli_connect( $servidor, $usuario, $contrasena ) or die ("No se ha podido conectar al servidor de Base de datos");
    $db = mysqli_select_db( $conexion, $basededatos );
    $dir = str_replace("\\","/",$_FILES['fileToUpload']['tmp_name']);
    $fecha = "'". date('Y-m-d H:m:s')."'";
    $periodo = $_POST['periodo'];

    $sql="INSERT INTO oncargas(CAR_FEC_REGISTRO, ON_PERIODOS_ID, CAR_TIPO, CAR_OBSERVACION)
          VALUES($fecha, $periodo ,'P','')";
    if(mysqli_query( $conexion, $sql )) {
        $sql="SELECT MAX(id) as id FROM oncargas Where on_periodos_id = ".$periodo;
        $result = mysqli_query( $conexion, $sql );
        while($row = mysqli_fetch_assoc($result))
            $carId = $row['id'];
        $enclosed = '"';
        $sql = "LOAD DATA LOCAL INFILE '" . $dir . "' 
        IGNORE INTO TABLE onproductividad
            FIELDS TERMINATED BY ',' 
            ENCLOSED BY '".$enclosed."'
        LINES TERMINATED BY '\n' 
        IGNORE 1 LINES (
          siteid,
          subsiteid,
          i3user,
          queue,
          totEnteredACD,
          totnAnswered,
           totnAnsweredACD ,
           totnAbandonedACD ,
           totnFlowOutAcd ,
           totnTransferedACD ,
           totnIntToIntACD ,
           totnExtToInt ,
           totnIntToExt ,
           totTTalkACD ,
           totTACW ,
           totTHoldACD ,
           totTAgentLoggedin ,
           totTAgentLoggedin2 ,
           totTAgentAvailable ,
           totTAgentACDLoggedIn ,
           totnExttoIntNONACD ,
           totnInttoExtNONACD ,
           totTExttoIntNONACD ,
           totTInttoExtNONACD ,
           Active ,
           IsPrivate ,
           IndivTypeID ,
           TitleID ,
           IndivID ,
           LastName ,
           FirstName ,
           MiddleName ,
           DisplayUserName ,
           JobTitle ,
           ICUserID ,
           ExtID ,
           ExtSource ,
           rank)
        SET fecha='". date('Y-m-d H:i:s') ."', ON_CARGAS_ID='" . $carId . "', PRO_OBSERVACIONES='OK';
     ";


       // var_dump($sql);
        if(mysqli_query( $conexion, $sql )){
            $i = mysqli_affected_rows($conexion);
            //$j = mysqli_warning_count($conexion);
            //$e = mysqli_get_warnings($conexion);
            //$a = 0;
            //$b = 0;

            if($i > 0){
                echo "OK";
            }else{
                    echo "NO";
                $sql = "DELETE FROM oncargas where id = " . $carId;
                if (mysqli_query($conexion, $sql)) {
                        //echo "ERROR AL ELIMINAR LA CARGA";
                } else {
                        echo mysqli_error($conexion);
                }
            }
            $sql="UPDATE onperiodos SET PER_EST_PRO = 'CARGADO' WHERE id = ".$periodo;
            if(mysqli_query( $conexion, $sql )) {
                echo "OK";
            }else{
                echo "NO";
            }
        }else{

            $sql="DELETE FROM oncargas where id = ". $carId;
            if(mysqli_query( $conexion, $sql )) {
                echo mysqli_error($conexion);
            }else{
                echo mysqli_error($conexion);
            }
        }
    }else{
        echo mysqli_error($conexion);
    }

?>