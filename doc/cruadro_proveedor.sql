﻿Select ot1.fecha, ot1.i3user, CONCAT(ot1.nombres," ", ot1.apellidos) as nombre,
	(SELECT SUM(ot2.duracion) FROM ontiempos ot2 WHERE ot2.estado2 = "Llamada" and ot2.i3user= ot1.i3user Group By ot2.on_cargas_id, ot2.i3user, ot2.estado2 ) as Llamada,
  (SELECT SUM(ot2.duracion) FROM ontiempos ot2 WHERE ot2.estado2 = "ACW" and ot2.i3user= ot1.i3user Group By ot2.on_cargas_id,ot2.i3user, ot2.estado2 ) as ACW, 
  (SELECT SUM(ot2.duracion) FROM ontiempos ot2 WHERE ot2.estado2 = "Disponible" and ot2.i3user= ot1.i3user Group By ot2.on_cargas_id,ot2.i3user, ot2.estado2 ) as Disponible,
  (SELECT SUM(ot2.duracion) FROM ontiempos ot2 WHERE ot2.estado2 = "Retroalimentacion" and ot2.i3user= ot1.i3user Group By ot2.on_cargas_id,ot2.i3user, ot2.estado2 ) as Retroalimentacion,
 (SELECT SUM(ot2.duracion) FROM ontiempos ot2 WHERE ot2.estado2 = "Administrativo" and ot2.i3user= ot1.i3user Group By ot2.on_cargas_id,ot2.i3user, ot2.estado2 ) as Administrativo,
 (SELECT SUM(ot2.duracion) FROM ontiempos ot2 WHERE ot2.estado2 = "Recarga" and ot2.i3user= ot1.i3user Group By ot2.on_cargas_id, ot2.i3user, ot2.estado2 ) as Recarga,
 (SELECT SUM(ot2.duracion) FROM ontiempos ot2 WHERE ot2.estado2 = "Not Ready" and ot2.i3user= ot1.i3user Group By ot2.on_cargas_id,ot2.i3user, ot2.estado2 ) as NotReady
 from ontiempos ot1
Where on_cargas_id = 4
Group By on_cargas_id, i3user