﻿# Host: localhost  (Version 5.7.9-log)
# Date: 2018-06-11 00:00:14
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "fos_user"
#

DROP TABLE IF EXISTS `fos_user`;
CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `USU_LIDER` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USU_NOMAPE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USU_NOMBRES` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USU_APELLIDOS` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USU_APENOM` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USU_COLA` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "fos_user"
#

INSERT INTO `fos_user` VALUES (1,'jmejia','jmejia','jmejia@ppm.com.ec','jmejia@ppm.com.ec',1,'sy2nf7s9kn4woc8gocsg8k84ok4o4gw','$2y$13$sy2nf7s9kn4woc8gocsg8eWOftbnypgRlm6mftABH0TdQsLhPupM2','2018-06-10 22:08:50',0,0,NULL,NULL,NULL,'a:1:{i:0;s:13:\"ADMINISTRADOR\";}',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'GEAKJARV','geakjarv','GEAKJARV@aditec.ec','geakjarv@aditec.ec',1,'6c401109sps0o048g8k00k84o88ogsg','$2y$13$6c401109sps0o048g8k00eIW1wcjzC7wB2fqSc2glwJeGeLTq1Fxy',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'FRANKLIN PAUCAY','KERLY MARILIN JARA VERA','KERLY MARILIN','JARA VERA','JARA VERA KERLY MARILIN','MIGRACIÓN');

#
# Structure for table "oncampanas"
#

DROP TABLE IF EXISTS `oncampanas`;
CREATE TABLE `oncampanas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CAM_NOMBRE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CAM_ESTADO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "oncampanas"
#

INSERT INTO `oncampanas` VALUES (1,'CAT_RenovOut','1'),(2,'CAT_Mig','1'),(3,'CAT_Vent_Hogar','1'),(4,'CAT_Por','1'),(5,'CAT_RedesSociales','1'),(6,'CAT_Back','1');

#
# Structure for table "onestados"
#

DROP TABLE IF EXISTS `onestados`;
CREATE TABLE `onestados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `EST_CODIGO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EST_NOMBRE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EST_TIEMPO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "onestados"
#

INSERT INTO `onestados` VALUES (38,'at a training session','Retroalimentacion',1200),(39,'at lunch','Recarga',1800),(40,'available','Disponible',0),(41,'available, follow-me','ACW',2400),(42,'available, no acd','Administrativo',2700),(43,'awaiting callback','Disponible',0),(44,'away from desk','Not ready',0),(45,'campaign call','Llamada',0),(46,'coaching_en_linea','Administrativo',2700),(47,'do not disturb','Not ready',0),(48,'follow up','ACW',2400),(49,'gone home','Not ready',0),(50,'in a meeting','Not ready',0),(51,'invalid status','Not ready',0),(52,'on vacation','Not ready',0),(53,'out of the office','Not ready',0),(54,'out of town','Not ready',0),(55,'working at home','Not ready',0),(56,'acdagentnotanswering','Not ready',0);

#
# Structure for table "onperiodos"
#

DROP TABLE IF EXISTS `onperiodos`;
CREATE TABLE `onperiodos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PER_NOMBRE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PER_FECHA` datetime NOT NULL,
  `PER_EST_PRO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PER_EST_TIE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PER_ESTADO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PER_OBSERVACION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ON_CAMPANAS_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F723F5DED7D4BA6C` (`ON_CAMPANAS_ID`),
  CONSTRAINT `FK_F723F5DED7D4BA6C` FOREIGN KEY (`ON_CAMPANAS_ID`) REFERENCES `oncampanas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "onperiodos"
#


#
# Structure for table "oncuadromo"
#

DROP TABLE IF EXISTS `oncuadromo`;
CREATE TABLE `oncuadromo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `a` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `b` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `d` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `e` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `g` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `h` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `i` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `j` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `k` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `n` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `p` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `q` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `r` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `t` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `v` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `y` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `z` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ab` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ac` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ON_PERIODOS_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_970AB532B732F926` (`ON_PERIODOS_ID`),
  CONSTRAINT `FK_970AB532B732F926` FOREIGN KEY (`ON_PERIODOS_ID`) REFERENCES `onperiodos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "oncuadromo"
#


#
# Structure for table "oncargas"
#

DROP TABLE IF EXISTS `oncargas`;
CREATE TABLE `oncargas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CAR_FEC_REGISTRO` datetime NOT NULL,
  `CAR_TIPO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CAR_OBSERVACION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ON_PERIODOS_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A416B61DB732F926` (`ON_PERIODOS_ID`),
  CONSTRAINT `FK_A416B61DB732F926` FOREIGN KEY (`ON_PERIODOS_ID`) REFERENCES `onperiodos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "oncargas"
#


#
# Structure for table "onproductividad"
#

DROP TABLE IF EXISTS `onproductividad`;
CREATE TABLE `onproductividad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `siteid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subsiteid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `i3user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totEnteredACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnAnswered` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnAnsweredACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnAbandonedACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnFlowOutAcd` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnTransferedACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnIntToIntACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnExtToInt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnIntToExt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totTTalkACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totTACW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totTHoldACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totTAgentLoggedin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totTAgentLoggedin2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totTAgentAvailable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totTAgentACDLoggedIn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnExttoIntNONACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totnInttoExtNONACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totTExttoIntNONACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totTInttoExtNONACD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IsPrivate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IndivTypeID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TitleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IndivID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FirstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MiddleName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DisplayUserName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `JobTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ICUserID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ExtID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ExtSource` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRO_OBSERVACIONES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ON_CARGAS_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_755140FEDAFDF7C4` (`ON_CARGAS_ID`),
  CONSTRAINT `FK_755140FEDAFDF7C4` FOREIGN KEY (`ON_CARGAS_ID`) REFERENCES `oncargas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "onproductividad"
#


#
# Structure for table "ontiempos"
#

DROP TABLE IF EXISTS `ontiempos`;
CREATE TABLE `ontiempos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `i3user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  `TIE_OBSERVACIONES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ON_CARGAS_ID` int(11) DEFAULT NULL,
  `estado2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EST_TIEMPO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7068AAA5DAFDF7C4` (`ON_CARGAS_ID`),
  CONSTRAINT `FK_7068AAA5DAFDF7C4` FOREIGN KEY (`ON_CARGAS_ID`) REFERENCES `oncargas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63176 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "ontiempos"
#

