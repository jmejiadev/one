﻿select ot1.fecha, ot1.i3user, CONCAT(ot1.nombres," ", ot1.apellidos) as nombre, CONCAT(FLOOR((ot2.llamadas/60)/60),":",FLOOR((ot2.llamadas/60)%60),":",ot2.llamadas%60)as llamadas, ot3.acw, ot4.disponibles, ot5.retoralimentacion, ot6.administrativo, ot7.recarga, ot8.notready
from ontiempos ot1
left join (SELECT ot2.i3user,  SUM(ot2.duracion) as llamadas FROM ontiempos ot2 WHERE ot2.estado2 = "llamada" and ot2.on_cargas_id = 4 Group By ot2.on_cargas_id, ot2.i3user, ot2.estado2 ) as ot2 ON ot1.i3user = ot2.i3user
left join (SELECT ot3.i3user,  SUM(ot3.duracion) as acw FROM ontiempos ot3 WHERE ot3.estado2 = "ACW" and ot3.on_cargas_id = 4 Group By ot3.on_cargas_id, ot3.i3user, ot3.estado2 ) as ot3 ON ot1.i3user = ot3.i3user
left join (SELECT ot4.i3user, SUM(ot4.duracion) as disponibles FROM ontiempos ot4 WHERE ot4.estado2 = "Disponible" and ot4.on_cargas_id = 4 Group By ot4.on_cargas_id,ot4.i3user, ot4.estado2 ) as ot4 ON ot1.i3user = ot4.i3user
left join (SELECT ot5.i3user, SUM(ot5.duracion) as retoralimentacion FROM ontiempos ot5 WHERE ot5.estado2 = "Retroalimentacion" and ot5.on_cargas_id = 4 Group By ot5.on_cargas_id,ot5.i3user, ot5.estado2 ) as ot5 ON ot1.i3user = ot5.i3user
left join (SELECT ot6.i3user, SUM(ot6.duracion) as administrativo FROM ontiempos ot6 WHERE ot6.estado2 = "Administrativo" and ot6.on_cargas_id = 4 Group By ot6.on_cargas_id,ot6.i3user, ot6.estado2 ) as ot6 ON ot1.i3user = ot6.i3user
left join (SELECT ot7.i3user, SUM(ot7.duracion) as recarga FROM ontiempos ot7 WHERE ot7.estado2 = "Recarga" and ot7.on_cargas_id = 4 Group By ot7.on_cargas_id, ot7.i3user, ot7.estado2 ) as ot7 ON ot1.i3user = ot7.i3user
left join (SELECT ot8.i3user, SUM(ot8.duracion) as notready FROM ontiempos ot8 WHERE ot8.estado2 = "Not Ready" and ot8.on_cargas_id = 4 Group By ot8.on_cargas_id,ot8.i3user, ot8.estado2 ) as ot8 ON ot1.i3user = ot8.i3user
Where ot1.on_cargas_id = 4
Group By ot1.on_cargas_id, ot1.i3user