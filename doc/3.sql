﻿select 
ot1.fecha as A, 
(Select u.usu_lider From fos_user u Where u.username = ot1.i3user) as B, 
CONCAT(ot1.nombres," ", ot1.apellidos) as C, 
ot1.i3user as D,
(IFNULL(ot2.llamadas,0)/p1.h) as E,
(IFNULL(ot2.llamadas,0)/(IFNULL(ot2.llamadas,0)+IFNULL(ot3.acw,0)+IFNULL(ot4.disponibles, 0)+IFNULL(ot5.retroalimentacion,0)+IFNULL(ot6.administrativo, 0)+IFNULL(ot7.recarga,0)))*100 as F,
p1.w as G,
p1.f as H,
(p1.w + p1.f) as I,
p1.h as J,
"VALIDAR" as K,
IFNULL(ot2.llamadas,0) as L,
IFNULL(ot3.acw,0) as M,
IFNULL(ot4.disponibles, 0) as N,
IFNULL(ot5.retroalimentacion,0) as O, 
IFNULL(ot6.administrativo, 0) as P,
IFNULL(ot7.recarga,0)as Q , 
IFNULL(ot8.notready,0) as R,
(IFNULL(ot2.llamadas,0)+IFNULL(ot3.acw,0)+IFNULL(ot4.disponibles, 0)+IFNULL(ot5.retroalimentacion,0)+IFNULL(ot6.administrativo, 0)+IFNULL(ot7.recarga,0)) as S,
IFNULL(ot2.llamadas,0) as T,
IF(ot3.acw < (Select distinct est_tiempo from onEstados Where est_nombre = "acw"),ot3.acw,(Select distinct est_tiempo from onEstados Where est_nombre = "acw"))as U,
IFNULL(ot4.disponibles, 0) as V,
IF(IFNULL(ot5.retroalimentacion,0) < (Select distinct est_tiempo from onEstados Where est_nombre = "Retroalimentacion"),IFNULL(ot5.retroalimentacion,0),(Select distinct est_tiempo from onEstados Where est_nombre = "Retroalimentacion"))as W,
IF(IFNULL(ot6.administrativo,0) < (Select distinct est_tiempo from onEstados Where est_nombre = "Administrativo"),IFNULL(ot6.administrativo,0),(Select distinct est_tiempo from onEstados Where est_nombre = "Administrativo"))as X,
IF(IFNULL(ot7.recarga,0) < (Select distinct est_tiempo from onEstados Where est_nombre = "Recarga"),IFNULL(ot7.recarga,0),(Select distinct est_tiempo from onEstados Where est_nombre = "Recarga"))as Y,
IF(IFNULL(ot8.notready,0) < (Select distinct est_tiempo from onEstados Where est_nombre = "Not Ready"),IFNULL(ot8.notready,0),(Select distinct est_tiempo from onEstados Where est_nombre = "Not Ready"))as Z,
(IFNULL(ot2.llamadas,0) + 
IF(ot3.acw < (Select distinct est_tiempo from onEstados Where est_nombre = "acw"),ot3.acw,(Select distinct est_tiempo from onEstados Where est_nombre = "acw")) +
IFNULL(ot4.disponibles, 0) +
IF(IFNULL(ot5.retroalimentacion,0) < (Select distinct est_tiempo from onEstados Where est_nombre = "Retroalimentacion"),IFNULL(ot5.retroalimentacion,0),(Select distinct est_tiempo from onEstados Where est_nombre = "Retroalimentacion")) +
IF(IFNULL(ot6.administrativo,0) < (Select distinct est_tiempo from onEstados Where est_nombre = "Administrativo"),IFNULL(ot6.administrativo,0),(Select distinct est_tiempo from onEstados Where est_nombre = "Administrativo")) +
IF(IFNULL(ot7.recarga,0) < (Select distinct est_tiempo from onEstados Where est_nombre = "Recarga"),IFNULL(ot7.recarga,0),(Select distinct est_tiempo from onEstados Where est_nombre = "Recarga")) +
IF(IFNULL(ot8.notready,0) < (Select distinct est_tiempo from onEstados Where est_nombre = "Not Ready"),IFNULL(ot8.notready,0),(Select distinct est_tiempo from onEstados Where est_nombre = "Not Ready"))
)as AA
from ontiempos ot1
left join (SELECT ot2.i3user,  SUM(ot2.duracion) as llamadas FROM ontiempos ot2 WHERE ot2.estado2 = "llamada" and ot2.on_cargas_id = 4 Group By ot2.on_cargas_id, ot2.i3user, ot2.estado2 ) as ot2 ON ot1.i3user = ot2.i3user
left join (SELECT ot3.i3user,  SUM(ot3.duracion) as acw FROM ontiempos ot3 WHERE ot3.estado2 = "ACW" and ot3.on_cargas_id = 4 Group By ot3.on_cargas_id, ot3.i3user, ot3.estado2 ) as ot3 ON ot1.i3user = ot3.i3user
left join (SELECT ot4.i3user, SUM(ot4.duracion) as disponibles FROM ontiempos ot4 WHERE ot4.estado2 = "Disponible" and ot4.on_cargas_id = 4 Group By ot4.on_cargas_id,ot4.i3user, ot4.estado2 ) as ot4 ON ot1.i3user = ot4.i3user
left join (SELECT ot5.i3user, SUM(ot5.duracion) as retroalimentacion FROM ontiempos ot5 WHERE ot5.estado2 = "Retroalimentacion" and ot5.on_cargas_id = 4 Group By ot5.on_cargas_id,ot5.i3user, ot5.estado2 ) as ot5 ON ot1.i3user = ot5.i3user
left join (SELECT ot6.i3user, SUM(ot6.duracion) as administrativo FROM ontiempos ot6 WHERE ot6.estado2 = "Administrativo" and ot6.on_cargas_id = 4 Group By ot6.on_cargas_id,ot6.i3user, ot6.estado2 ) as ot6 ON ot1.i3user = ot6.i3user
left join (SELECT ot7.i3user, SUM(ot7.duracion) as recarga FROM ontiempos ot7 WHERE ot7.estado2 = "Recarga" and ot7.on_cargas_id = 4 Group By ot7.on_cargas_id, ot7.i3user, ot7.estado2 ) as ot7 ON ot1.i3user = ot7.i3user
left join (SELECT ot8.i3user, SUM(ot8.duracion) as notready FROM ontiempos ot8 WHERE ot8.estado2 = "Not Ready" and ot8.on_cargas_id = 4 Group By ot8.on_cargas_id,ot8.i3user, ot8.estado2 ) as ot8 ON ot1.i3user = ot8.i3user
left join (Select p1.i3user,  SUM(totEnteredACD)as f, SUM(totnAnsweredACD) as h, SUM(totnInttoExtNONACD) as w from onproductividad p1 WHERE p1.on_cargas_id = 2 Group By i3user) as p1 ON ot1.i3user = p1.i3user
Where ot1.on_cargas_id = 4
Group By ot1.on_cargas_id, ot1.i3user


