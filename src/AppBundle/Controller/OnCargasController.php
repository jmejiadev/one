<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OnCargas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Oncarga controller.
 *
 * @Route("oncargas")
 */
class OnCargasController extends Controller
{
    /**
     * Lists all onCarga entities.
     *
     * @Route("/", name="oncargas_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $onCargas = $em->getRepository('AppBundle:OnCargas')->findAll();

        return $this->render('oncargas/index.html.twig', array(
            'onCargas' => $onCargas,
        ));
    }

    /**
     * Creates a new onCarga entity.
     *
     * @Route("/new", name="oncargas_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $onCarga = new Oncarga();
        $form = $this->createForm('AppBundle\Form\OnCargasType', $onCarga);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($onCarga);
            $em->flush();

            return $this->redirectToRoute('oncargas_show', array('id' => $onCarga->getId()));
        }

        return $this->render('oncargas/new.html.twig', array(
            'onCarga' => $onCarga,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a onCarga entity.
     *
     * @Route("/{id}", name="oncargas_show")
     * @Method("GET")
     */
    public function showAction(OnCargas $onCarga)
    {
        $deleteForm = $this->createDeleteForm($onCarga);

        return $this->render('oncargas/show.html.twig', array(
            'onCarga' => $onCarga,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing onCarga entity.
     *
     * @Route("/{id}/edit", name="oncargas_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, OnCargas $onCarga)
    {
        $deleteForm = $this->createDeleteForm($onCarga);
        $editForm = $this->createForm('AppBundle\Form\OnCargasType', $onCarga);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('oncargas_edit', array('id' => $onCarga->getId()));
        }

        return $this->render('oncargas/edit.html.twig', array(
            'onCarga' => $onCarga,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a onCarga entity.
     *
     * @Route("/{id}", name="oncargas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, OnCargas $onCarga)
    {
        $form = $this->createDeleteForm($onCarga);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($onCarga);
            $em->flush();
        }

        return $this->redirectToRoute('oncargas_index');
    }

    /**
     * Creates a form to delete a onCarga entity.
     *
     * @param OnCargas $onCarga The onCarga entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OnCargas $onCarga)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('oncargas_delete', array('id' => $onCarga->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
