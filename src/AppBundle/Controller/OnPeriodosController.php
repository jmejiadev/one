<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OnPeriodos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * OnPeriodos controller.
 *
 * @Route("onperiodos")
 */
class OnPeriodosController extends Controller
{
    /**
     * Lists all onPeriodo entities.
     *
     * @Route("/", name="onperiodos_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $onPeriodos = $em->getRepository('AppBundle:OnPeriodos')->getPeriodosAll();

        return $this->render('onperiodos/index.html.twig', array(
            'onPeriodos' => $onPeriodos,
        ));
    }
    
    

    /**
     * Creates a new onPeriodo entity.
     *
     * @Route("/new", name="onperiodos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $onPeriodos = $em->getRepository('AppBundle:OnPeriodos')->getPeriodosAll();
        $onCampanas = $em->getRepository('AppBundle:OnCampanas')->getCampanasAll();

        $onPeriodo = new OnPeriodos();
        $form = $this->createForm('AppBundle\Form\OnPeriodosType', $onPeriodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($onPeriodo);
            $em->flush();

            return $this->redirectToRoute('onperiodos_show', array('id' => $onPeriodo->getId()));
        }

        return $this->render('onperiodos/new.html.twig', array(
            'onPeriodo' => $onPeriodo,
            'onPeriodos' => $onPeriodos,
            'onCampanas' => $onCampanas,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a onPeriodo entity.
     *
     * @Route("/{id}", name="onperiodos_show")
     * @Method("GET")
     */
    public function showAction(OnPeriodos $onPeriodo)
    {
        $deleteForm = $this->createDeleteForm($onPeriodo);
        $usuario = $this->container->getParameter('database_user');
        $contrasena = $this->container->getParameter('database_password');
        $servidor = $this->container->getParameter('database_host');
        $basededatos = $this->container->getParameter('database_name');
        return $this->render('onperiodos/show.html.twig', array(
            'onPeriodo' => $onPeriodo,
            'usuario' => $usuario,
            'contrasena' => $contrasena,
            'servidor' => $servidor,
            'basededatos' => $basededatos,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing onPeriodo entity.
     *
     * @Route("/{id}/edit", name="onperiodos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, OnPeriodos $onPeriodo)
    {
        $deleteForm = $this->createDeleteForm($onPeriodo);
        $editForm = $this->createForm('AppBundle\Form\OnPeriodosType', $onPeriodo);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('onperiodos_edit', array('id' => $onPeriodo->getId()));
        }

        return $this->render('onperiodos/edit.html.twig', array(
            'onPeriodo' => $onPeriodo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a onPeriodo entity.
     *
     * @Route("/{id}", name="onperiodos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, OnPeriodos $onPeriodo)
    {
        $form = $this->createDeleteForm($onPeriodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($onPeriodo);
            $em->flush();
        }

        return $this->redirectToRoute('onperiodos_index');
    }

    /**
     * Creates a form to delete a onPeriodo entity.
     *
     * @param OnPeriodos $onPeriodo The onPeriodo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OnPeriodos $onPeriodo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('onperiodos_delete', array('id' => $onPeriodo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
