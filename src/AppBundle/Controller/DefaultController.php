<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();
//        var_dump($user);
        if (false === $this->get('security.context')->isGranted('ROLE_USER')) {
                return $this->redirect($this->generateUrl('fos_user_security_login',array(
                    'user'=>$user,
                )));
        }else{
//             $user = $this->get('security.context')->getToken()->getUser();
//            var_dump($user);
            return $this->render('base.html.twig', array(
                'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
                'user'=>$user,
            ));
        
        }
    }
}
