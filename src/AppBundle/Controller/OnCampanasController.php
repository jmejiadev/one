<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OnCampanas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Oncampana controller.
 *
 * @Route("oncampanas")
 */
class OnCampanasController extends Controller
{
    /**
     * Lists all onCampana entities.
     *
     * @Route("/", name="oncampanas_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $onCampanas = $em->getRepository('AppBundle:OnCampanas')->getCampanasAll();

        return $this->render('oncampanas/index.html.twig', array(
            'onCampanas' => $onCampanas,
        ));
    }

    /**
     * Creates a new onCampana entity.
     *
     * @Route("/new", name="oncampanas_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $onCampanas = $em->getRepository('AppBundle:OnCampanas')->getCampanasAll();
        $onCampana = new OnCampanas();
        $form = $this->createForm('AppBundle\Form\OnCampanasType', $onCampana);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($onCampana);
            $em->flush();

            return $this->redirectToRoute('oncampanas_index');
        }

        return $this->render('oncampanas/new.html.twig', array(
            'onCampana' => $onCampana,
            'onCampanas' => $onCampanas,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a onCampana entity.
     *
     * @Route("/{id}", name="oncampanas_show")
     * @Method("GET")
     */
    public function showAction(OnCampanas $onCampana)
    {
        $deleteForm = $this->createDeleteForm($onCampana);

        return $this->render('oncampanas/show.html.twig', array(
            'onCampana' => $onCampana,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing onCampana entity.
     *
     * @Route("/{id}/edit", name="oncampanas_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, OnCampanas $onCampana)
    {
        $em = $this->getDoctrine()->getManager();

        $onCampanas = $em->getRepository('AppBundle:OnCampanas')->getCampanasEdit($onCampana->getId());
        $deleteForm = $this->createDeleteForm($onCampana);
        $editForm = $this->createForm('AppBundle\Form\OnCampanasType', $onCampana);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('oncampanas_index');
        }

        return $this->render('oncampanas/edit.html.twig', array(
            'onCampana' => $onCampana,
            'onCampanas' => $onCampanas,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a onCampana entity.
     *
     * @Route("/{id}", name="oncampanas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, OnCampanas $onCampana)
    {
        $form = $this->createDeleteForm($onCampana);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($onCampana);
            $em->flush();
        }

        return $this->redirectToRoute('oncampanas_index');
    }

    /**
     * Creates a form to delete a onCampana entity.
     *
     * @param OnCampanas $onCampana The onCampana entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OnCampanas $onCampana)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('oncampanas_delete', array('id' => $onCampana->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
