<?php

namespace AppBundle\Controller;


use AppBundle\Entity\OnCuadromo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Oncuadromo controller.
 *
 * @Route("oncuadromo")
 */
class OnCuadromoController extends Controller
{
    /**
     * genera cuadromo.
     *
     * @Route("/genera/cuadro", name="oncuadromo_generar")
     * @Method({"GET", "POST"})
     */
    public function generarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $perId = $_POST['perId'];
        $periodo = $em->getRepository('AppBundle:OnPeriodos')->find($perId);
        $carP = $em->getRepository('AppBundle:OnCargas')->getCargaByTipo($perId,"P");
        $carT = $em->getRepository('AppBundle:OnCargas')->getCargaByTipo($perId,"T");
        $exiCuadro = $em->getRepository('AppBundle:OnCuadromo')->findBy(array('onperiodos'=>$perId));
        if($exiCuadro){
            foreach ($exiCuadro as $cuadro) {
                $em->remove($cuadro);
            }
            $em->flush();
//            $response = json_encode("YA EXISTE");
        }

//            AGRUPO TIEMPOS
        $tiempos = $em->getRepository('AppBundle:OnCuadromo')->getCuadro($carP[0]['id'],$carT[0]['id']);
        foreach ($tiempos as $value) {
            $cuadro = new OnCuadromo();
            $cuadro->setA($value['A']);
            $cuadro->setB($value['B']);
            $cuadro->setC($value['C']);
            $cuadro->setD($value['D']);
            $cuadro->setE($value['E']);
            $cuadro->setF($value['F']);
            $cuadro->setG($value['G']);
            $cuadro->setH($value['H']);
            $cuadro->setI($value['I']);
            $cuadro->setJ($value['J']);
            $cuadro->setK($value['K']);
            $cuadro->setL($value['L']);
            $cuadro->setM($value['M']);
            $cuadro->setN($value['N']);
            $cuadro->setO($value['O']);
            $cuadro->setP($value['P']);
            $cuadro->setQ($value['Q']);
            $cuadro->setR($value['R']);
            $cuadro->setS($value['S']);
            $cuadro->setT($value['T']);
            $cuadro->setU($value['U']);
            $cuadro->setV($value['V']);
            $cuadro->setW($value['W']);
            $cuadro->setX($value['X']);
            $cuadro->setY($value['Y']);
            $cuadro->setZ($value['Z']);
            $cuadro->setAa($value['AA']);
            $cuadro->setAb('');
            $cuadro->setAc('');
            $cuadro->setOnperiodos($periodo);
            $em->persist($cuadro);
        }

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("liuggio")
            ->setLastModifiedBy("Giulio De Donato")
            ->setTitle("Office 2005 XLSX Test Document")
            ->setSubject("Office 2005 XLSX Test Document")
            ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
            ->setKeywords("office 2005 openxml php")
            ->setCategory("Test result file");


        $row = 1;
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A'.$row, 'Fecha')
            ->setCellValue('B'.$row, 'Lider')
            ->setCellValue('C'.$row, 'Asesores')
            ->setCellValue('D'.$row, 'Usuario')
            ->setCellValue('E'.$row, 'TCP')
            ->setCellValue('F'.$row, 'Ocupación')
            ->setCellValue('G'.$row, 'Total llamadas manuales')
            ->setCellValue('H'.$row, 'Total llamadas marcador')
            ->setCellValue('I'.$row, 'Total llamadas Realizadas')
            ->setCellValue('J'.$row, 'Total llamadas contestadas')
            ->setCellValue('K'.$row, 'Horas ACD presupuestadas Claro')
            ->setCellValue('L'.$row, 'Talking time')
            ->setCellValue('M'.$row, 'Tiempo ACW')
            ->setCellValue('N'.$row, 'Disponible básico')
            ->setCellValue('O'.$row, 'Disponible Retroalimentación')
            ->setCellValue('P'.$row, 'Disponible Administrativo')
            ->setCellValue('Q'.$row, 'Disponible Recarga')
            ->setCellValue('R'.$row, 'No disponible')
            ->setCellValue('S'.$row, 'Horas ACD generadas por proveedor')
            ->setCellValue('T'.$row, 'Validación Talking time')
            ->setCellValue('U'.$row, 'Validacion ACW')
            ->setCellValue('V'.$row, 'Validación Disponible básico')
            ->setCellValue('W'.$row, 'Validación Disponible Retroalimentación')
            ->setCellValue('X'.$row, 'Validación Disponible Administrativo')
            ->setCellValue('Y'.$row, 'Validación Disponible Recarga')
            ->setCellValue('Z'.$row, 'Validación No disponible')
            ->setCellValue('AA'.$row,'Horas ACD autorizadas para pago - Claro');
        foreach ($tiempos as $value) {
            $row=$row+1;
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'.$row, $value['A'])
                ->setCellValue('B'.$row, $value['B'])
                ->setCellValue('C'.$row, $value['C'])
                ->setCellValue('D'.$row, $value['D'])
                ->setCellValue('E'.$row, $value['E'])
                ->setCellValue('F'.$row, $value['F'])
                ->setCellValue('G'.$row, $value['G'])
                ->setCellValue('H'.$row, $value['H'])
                ->setCellValue('I'.$row, $value['I'])
                ->setCellValue('J'.$row, $value['J'])
                ->setCellValue('K'.$row, $value['K'])
                ->setCellValue('L'.$row, $value['L'])
                ->setCellValue('M'.$row, $value['M'])
                ->setCellValue('N'.$row, $value['N'])
                ->setCellValue('O'.$row, $value['O'])
                ->setCellValue('P'.$row, $value['P'])
                ->setCellValue('Q'.$row, $value['Q'])
                ->setCellValue('R'.$row, $value['R'])
                ->setCellValue('S'.$row, $value['S'])
                ->setCellValue('T'.$row, $value['T'])
                ->setCellValue('U'.$row, $value['U'])
                ->setCellValue('V'.$row, $value['V'])
                ->setCellValue('W'.$row, $value['W'])
                ->setCellValue('X'.$row, $value['X'])
                ->setCellValue('Y'.$row, $value['Y'])
                ->setCellValue('Z'.$row, $value['Z'])
                ->setCellValue('AA'.$row, $value['AA']);
        }
        $phpExcelObject->getActiveSheet()->setTitle('Simple');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
//        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
//        $dispositionHeader = $response->headers->makeDisposition(
//            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
//            'stream-file.xls'
//        );
//        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
//        $response->headers->set('Pragma', 'public');
//        $response->headers->set('Cache-Control', 'maxage=1');
//        $response->headers->set('Content-Disposition', $dispositionHeader);
//
        $writer->save('cuadros/'.$periodo->getPerNombre().'.xls');
//        return $response;
        $periodo->setPerEstado("GENERADO");
        $em->persist($periodo);
        $em->flush();
        $response = json_encode($periodo->getPerNombre().'.xls');

        return new Response($response,200, array( 'Content-Type' => 'application/json' ));
    }
    /**
     * genera DASHBOARD.
     *
     * @Route("/genera-dashboards", name="oncuadromo_dashboard")
     * @Method({"GET", "POST"})
     */
    public function dashbordAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $perId = $_POST['perId'];
        $datDas1 = $em->getRepository('AppBundle:OnCuadromo')->getDash1($perId);
        $datDas2 = $em->getRepository('AppBundle:OnCuadromo')->getDash2($perId);
        $datDas3 = $em->getRepository('AppBundle:OnCuadromo')->getDash3($perId);
        $datDas4 = $em->getRepository('AppBundle:OnCuadromo')->getDash4($perId);
        $datDas5 = $em->getRepository('AppBundle:OnCuadromo')->getDash5($perId);
        $dash[0] = $datDas1;
        $dash[1] = $datDas2;
        $dash[2] = $datDas3;
        $dash[3] = $datDas4;
        $dash[4] = $datDas5;
        $response = json_encode($dash);
        
        return new Response($response,200, array( 'Content-Type' => 'application/json' ));
    }
    /** 
     * Lists all onCuadromo entities.
     *
     * @Route("/", name="oncuadromo_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $onPeriodos = $em->getRepository('AppBundle:OnPeriodos')->getPeriodosEstado('GENERADO');


        return $this->render('oncuadromo/index.html.twig', array(
            'onPeriodos' => $onPeriodos,
        ));
    }

    /**
     * Creates a new onCuadromo entity.
     *
     * @Route("/new", name="oncuadromo_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $onCuadromo = new Oncuadromo();
        $form = $this->createForm('AppBundle\Form\OnCuadromoType', $onCuadromo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($onCuadromo);
            $em->flush();

            return $this->redirectToRoute('oncuadromo_show', array('id' => $onCuadromo->getId()));
        }

        return $this->render('oncuadromo/new.html.twig', array(
            'onCuadromo' => $onCuadromo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a onCuadromo entity.
     *
     * @Route("/{id}", name="oncuadromo_show")
     * @Method("GET")
     */
    public function showAction(OnCuadromo $onCuadromo)
    {
        $deleteForm = $this->createDeleteForm($onCuadromo);

        return $this->render('oncuadromo/show.html.twig', array(
            'onCuadromo' => $onCuadromo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing onCuadromo entity.
     *
     * @Route("/{id}/edit", name="oncuadromo_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, OnCuadromo $onCuadromo)
    {
        $deleteForm = $this->createDeleteForm($onCuadromo);
        $editForm = $this->createForm('AppBundle\Form\OnCuadromoType', $onCuadromo);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('oncuadromo_edit', array('id' => $onCuadromo->getId()));
        }

        return $this->render('oncuadromo/edit.html.twig', array(
            'onCuadromo' => $onCuadromo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a onCuadromo entity.
     *
     * @Route("/{id}", name="oncuadromo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, OnCuadromo $onCuadromo)
    {
        $form = $this->createDeleteForm($onCuadromo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($onCuadromo);
            $em->flush();
        }

        return $this->redirectToRoute('oncuadromo_index');
    }

    /**
     * Creates a form to delete a onCuadromo entity.
     *
     * @param OnCuadromo $onCuadromo The onCuadromo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OnCuadromo $onCuadromo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('oncuadromo_delete', array('id' => $onCuadromo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
