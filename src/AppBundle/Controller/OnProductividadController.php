<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OnCargas;
use AppBundle\Entity\OnPeriodos;
use AppBundle\Entity\OnProductividad;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use League\Csv\Reader;
/**
 * Onproductividad controller.
 *
 * @Route("onproductividad")
 */
class OnProductividadController extends Controller
{
    
     /**
     * import productividad.
     *
     * @Route("/borrar/productividad", name="onproductividad_borrar")
     * @Method({"GET", "POST"})
     */
    public function borrarProductividadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $perId = $_POST['perId'];
        $exiCarga = $em->getRepository('AppBundle:OnCargas')->findBy(array('onperiodos'=>$perId,'carTipo'=>'P'));
        if($exiCarga){
            $prodExi = $em->getRepository('AppBundle:OnProductividad')->findBy(array('oncargas'=>$exiCarga[0]->getId()));
            foreach ($prodExi as $prod) {
                $em->remove($prod);
            }
            $em->remove($exiCarga[0]);
            $em->flush();
            
        }
         $periodo = $em->getRepository('AppBundle:OnPeriodos')->find($perId);
         $periodo->setPerEstPro("PENDIENTE");
         $em->persist($periodo);
         $em->flush();
        $response = json_encode("OK BORRADO");
        return new Response($response,200, array( 'Content-Type' => 'application/json' ));
    }


    /**
     * Lists all onProductividad entities.
     *
     * @Route("/", name="onproductividad_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $onProductividads = $em->getRepository('AppBundle:OnProductividad')->findAll();

        return $this->render('onproductividad/index.html.twig', array(
            'onProductividads' => $onProductividads,
        ));
    }

    /**
     * Creates a new onProductividad entity.
     *
     * @Route("/new", name="onproductividad_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $onProductividad = new Onproductividad();
        $form = $this->createForm('AppBundle\Form\OnProductividadType', $onProductividad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($onProductividad);
            $em->flush();

            return $this->redirectToRoute('onproductividad_show', array('id' => $onProductividad->getId()));
        }

        return $this->render('onproductividad/new.html.twig', array(
            'onProductividad' => $onProductividad,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a onProductividad entity.
     *
     * @Route("/{id}", name="onproductividad_show")
     * @Method("GET")
     */
    public function showAction(OnProductividad $onProductividad)
    {
        $deleteForm = $this->createDeleteForm($onProductividad);

        return $this->render('onproductividad/show.html.twig', array(
            'onProductividad' => $onProductividad,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing onProductividad entity.
     *
     * @Route("/{id}/edit", name="onproductividad_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, OnProductividad $onProductividad)
    {
        $deleteForm = $this->createDeleteForm($onProductividad);
        $editForm = $this->createForm('AppBundle\Form\OnProductividadType', $onProductividad);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('onproductividad_edit', array('id' => $onProductividad->getId()));
        }

        return $this->render('onproductividad/edit.html.twig', array(
            'onProductividad' => $onProductividad,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a onProductividad entity.
     *
     * @Route("/{id}", name="onproductividad_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, OnProductividad $onProductividad)
    {
        $form = $this->createDeleteForm($onProductividad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($onProductividad);
            $em->flush();
        }

        return $this->redirectToRoute('onproductividad_index');
    }

    /**
     * Creates a form to delete a onProductividad entity.
     *
     * @param OnProductividad $onProductividad The onProductividad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OnProductividad $onProductividad)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('onproductividad_delete', array('id' => $onProductividad->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
