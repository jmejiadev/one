<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OnTiempos;
use AppBundle\Entity\OnCargas;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use League\Csv\Reader;

/**
 * Ontiempo controller.
 *
 * @Route("ontiempos")
 */
class OnTiemposController extends Controller
{
    
    /**
     * import productividad.
     *
     * @Route("/borrar/borrar-tiempos", name="ontiempos_borrar")
     * @Method({"GET", "POST"})
     */
    public function borrarTiemposAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $perId = $_POST['perId'];
        $exiCarga = $em->getRepository('AppBundle:OnCargas')->findBy(array('onperiodos'=>$perId,'carTipo'=>'T'));
        if($exiCarga){
            $tiempoExi = $em->getRepository('AppBundle:OnTiempos')->findBy(array('oncargas'=>$exiCarga[0]->getId()));
            foreach ($tiempoExi as $tiempo) {
                $em->remove($tiempo);
            }
            $em->remove($exiCarga[0]);
            $em->flush();
        }
         $periodo = $em->getRepository('AppBundle:OnPeriodos')->find($perId);
         $periodo->setPerEstTie("PENDIENTE");
         $em->persist($periodo);
         $em->flush();
        $response = json_encode("OK BORRADO");
        return new Response($response,200, array( 'Content-Type' => 'application/json' ));
    }
    /**
     * import tiempos.
     *
     * @Route("/xls/tiempos", name="ontiempos_import_tiempos")
     * @Method({"GET", "POST"})
     */
    public function importTiemposAction(Request $request)
    {
//        var_dump($_POST);
        $em = $this->getDoctrine()->getManager();
//        $tipCarga = $_POST['tipCarga'];
        $fileName = $_POST['fileName'];
        $perId = $_POST['perId'];
        $periodo = $em->getRepository('AppBundle:OnPeriodos')->find($perId);
        $exiCarga = $em->getRepository('AppBundle:OnCargas')->findBy(array('onperiodos'=>$perId,'carTipo'=>'T'));
        if($exiCarga){
//            if($tipCarga == "completa"){
//                $tiempoExi = $em->getRepository('AppBundle:OnTiempos')->findBy(array('oncargas'=>$exiCarga[0]->getId()));
//                foreach ($tiempoExi as $tiempo) {
//                    $em->remove($tiempo);
//                }
//                $em->remove($exiCarga[0]);
//                $em->flush();
//                $carga = New OnCargas;
//                $carga->setCarFecRegistro(new \DateTime('now'));
//                $carga->setCarObservacion("TIEMPOS");
//                $carga->setCarTipo("T");
//                $carga->setOnperiodos($periodo);
//                $em->persist($carga);
//                $em->flush();
//            }else{
                $carga = $em->getRepository('AppBundle:OnCargas')->find($exiCarga[0]->getId());
//            }
        }else{
            $carga = New OnCargas;
            $carga->setCarFecRegistro(new \DateTime('now'));
            $carga->setCarObservacion("TIEMPOS");
            $carga->setCarTipo("T");
            $carga->setOnperiodos($periodo);
            $em->persist($carga);
            $em->flush();
        }

        $usuario = $this->container->getParameter('database_user');
        $contrasena = $this->container->getParameter('database_password');
        $servidor = $this->container->getParameter('database_host');
        $basededatos = $this->container->getParameter('database_name');
        $conexion = mysqli_connect( $servidor, $usuario, $contrasena ) or die ("No se ha podido conectar al servidor de Base de datos");
        $db = mysqli_select_db( $conexion, $basededatos );
      $objPHPExcel = $this->get('phpexcel')->createPHPExcelObject($this->get('kernel')->getRootDir() .'/../web/uploads/'.$fileName);
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV'); $objWriter->save('testExportFile.csv');


            $enclosed = '"';
        $dir = str_replace("\\","/",$this->get('kernel')->getRootDir());
        $sql = "LOAD DATA LOCAL INFILE '" . $dir .'/../web/testExportFile.csv'  ."' 
        IGNORE INTO TABLE ontiempos
            FIELDS TERMINATED BY ',' 
            ENCLOSED BY '".$enclosed."' 
        LINES TERMINATED BY '\n' 
        IGNORE 1 LINES (fecha,
        i3user,
        nombres,
        apellidos,
        estado,
        duracion)
        SET duracion = (TIME_TO_SEC(duracion)), estado2 = (Select est_nombre from onestados where est_codigo = estado LIMIT 1) , EST_TIEMPO = (Select est_tiempo from onestados where est_codigo = estado LIMIT 1) , ON_CARGAS_ID='" . $carga->getId() . "', TIE_OBSERVACIONES='OK';
     ";


            //var_dump($sql);
            if(mysqli_query( $conexion, $sql )){
                $i = mysqli_affected_rows($conexion);
                //$j = mysqli_warning_count($conexion);
                //$e = mysqli_get_warnings($conexion);
                //$a = 0;
                //$b = 0;
              //  var_dump($i);
                if($i > 0){
                //    echo "OK1";
                }else{
                  //  echo "NO";
                    $sql = "DELETE FROM oncargas where id = " . $carga->getId();
                    if (mysqli_query($conexion, $sql)) {
                    //    echo "ERROR AL ELIMINAR LA CARGA";
                    } else {
                      //  echo mysqli_error($conexion);
                    }
                }
            }else{

                $sql="DELETE FROM oncargas where id = ". $carga->getId();
                if(mysqli_query( $conexion, $sql )) {
                    //echo mysqli_error($conexion);
                }else{
                   // echo mysqli_error($conexion);
                }
            }
      mysqli_close($conexion);
      //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
      //$total_sheets=$objPHPExcel->getSheetCount();
      //$allSheetName=$objPHPExcel->getSheetNames();
      //$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
      //Se obtiene el número máximo de filas
      //$highestRow = $objWorksheet->getHighestRow();
      //Se obtiene el número máximo de columnas
      //$highestColumn = 'F';
      //      for ($row = 6; $row <= $highestRow; ++$row) {
      //           $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
      //           if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] != '')) {
      //               $tiempo = new OnTiempos;
      //               $tiempo->setFecha($dataRow[$row]['A']);
      //               $tiempo->setI3user($dataRow[$row]['B']);
      //               $tiempo->setNombres($dataRow[$row]['C']);
      //               $tiempo->setApellidos($dataRow[$row]['D']);
      //               $tiempo->setEstado($dataRow[$row]['E']);
      //               $estado = $em->getRepository('AppBundle:OnEstados')->findBy(array('estCodigo'=>$dataRow[$row]['E']));
      //               $tiempo->setEstado2($estado[0]->getEstNombre());
      //               $tiempo->setEstTiempo($estado[0]->getEstTiempo());
      //               list($h, $m, $s) = explode(':', $dataRow[$row]['F']);
      //               $segundos =  ($h * 3600) + ($m * 60) + $s;
      //               $tiempo->setDuracion($segundos);
      //               $tiempo->setTieObservaciones("");
      //               $tiempo->setOncargas($carga);
      //               $em->persist($tiempo);
      //           }
      //       }
             $periodo->setPerEstTie("CARGADO");
            $em->persist($periodo);
             $em->flush();
        $response = json_encode("OK");
        return new Response($response,200, array( 'Content-Type' => 'application/json' ));
    }
    
    /**
     * Lists all onTiempo entities.
     *
     * @Route("/", name="ontiempos_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $onTiempos = $em->getRepository('AppBundle:OnTiempos')->findAll();

        return $this->render('ontiempos/index.html.twig', array(
            'onTiempos' => $onTiempos,
        ));
    }

    /**
     * Creates a new onTiempo entity.
     *
     * @Route("/new", name="ontiempos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $onTiempo = new Ontiempo();
        $form = $this->createForm('AppBundle\Form\OnTiemposType', $onTiempo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($onTiempo);
            $em->flush();

            return $this->redirectToRoute('ontiempos_show', array('id' => $onTiempo->getId()));
        }

        return $this->render('ontiempos/new.html.twig', array(
            'onTiempo' => $onTiempo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a onTiempo entity.
     *
     * @Route("/{id}", name="ontiempos_show")
     * @Method("GET")
     */
    public function showAction(OnTiempos $onTiempo)
    {
        $deleteForm = $this->createDeleteForm($onTiempo);

        return $this->render('ontiempos/show.html.twig', array(
            'onTiempo' => $onTiempo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing onTiempo entity.
     *
     * @Route("/{id}/edit", name="ontiempos_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, OnTiempos $onTiempo)
    {
        $deleteForm = $this->createDeleteForm($onTiempo);
        $editForm = $this->createForm('AppBundle\Form\OnTiemposType', $onTiempo);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ontiempos_edit', array('id' => $onTiempo->getId()));
        }

        return $this->render('ontiempos/edit.html.twig', array(
            'onTiempo' => $onTiempo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a onTiempo entity.
     *
     * @Route("/{id}", name="ontiempos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, OnTiempos $onTiempo)
    {
        $form = $this->createDeleteForm($onTiempo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($onTiempo);
            $em->flush();
        }

        return $this->redirectToRoute('ontiempos_index');
    }

    /**
     * Creates a form to delete a onTiempo entity.
     *
     * @param OnTiempos $onTiempo The onTiempo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OnTiempos $onTiempo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ontiempos_delete', array('id' => $onTiempo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
