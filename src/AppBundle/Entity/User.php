<?php
// src/Acme/UserBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
//use AppBundle\Entity\Asignaturas;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @ORM\Column(name="USU_LIDER", type="string", length=200, nullable=true)
     */
    private $usuLider;
    
    /**
     * @var string
     * @ORM\Column(name="USU_NOMAPE", type="string", length=200, nullable=true)
     */
    private $usuNomape;
    
    /**
     * @var string
     * @ORM\Column(name="USU_NOMBRES", type="string", length=200, nullable=true)
     */
    private $usuNombres;
    
    /**
     * @var string
     * @ORM\Column(name="USU_APELLIDOS", type="string", length=200, nullable=true)
     */
    private $usuApellidos;
    
    /**
     * @var string
     * @ORM\Column(name="USU_APENOM", type="string", length=200, nullable=true)
     */
    private $usuApenom;
         
    /**
     * @var string
     * @ORM\Column(name="USU_COLA", type="string", length=200, nullable=true)
     */
    private $usuCola;
    
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Set usuLider
     *
     * @param string $usuLider
     *
     * @return User
     */
    public function setUsuLider($usuLider)
    {
        $this->usuLider = $usuLider;

        return $this;
    }

    /**
     * Get usuLider
     *
     * @return string
     */
    public function getUsuLider()
    {
        return $this->usuLider;
    }

    /**
     * Set usuNomape
     *
     * @param string $usuNomape
     *
     * @return User
     */
    public function setUsuNomape($usuNomape)
    {
        $this->usuNomape = $usuNomape;

        return $this;
    }

    /**
     * Get usuNomape
     *
     * @return string
     */
    public function getUsuNomape()
    {
        return $this->usuNomape;
    }

    /**
     * Set usuNombres
     *
     * @param string $usuNombres
     *
     * @return User
     */
    public function setUsuNombres($usuNombres)
    {
        $this->usuNombres = $usuNombres;

        return $this;
    }

    /**
     * Get usuNombres
     *
     * @return string
     */
    public function getUsuNombres()
    {
        return $this->usuNombres;
    }

    /**
     * Set usuApellidos
     *
     * @param string $usuApellidos
     *
     * @return User
     */
    public function setUsuApellidos($usuApellidos)
    {
        $this->usuApellidos = $usuApellidos;

        return $this;
    }

    /**
     * Get usuApellidos
     *
     * @return string
     */
    public function getUsuApellidos()
    {
        return $this->usuApellidos;
    }

    /**
     * Set usuApenom
     *
     * @param string $usuApenom
     *
     * @return User
     */
    public function setUsuApenom($usuApenom)
    {
        $this->usuApenom = $usuApenom;

        return $this;
    }

    /**
     * Get usuApenom
     *
     * @return string
     */
    public function getUsuApenom()
    {
        return $this->usuApenom;
    }

    /**
     * Set usuCola
     *
     * @param string $usuCola
     *
     * @return User
     */
    public function setUsuCola($usuCola)
    {
        $this->usuCola = $usuCola;

        return $this;
    }

    /**
     * Get usuCola
     *
     * @return string
     */
    public function getUsuCola()
    {
        return $this->usuCola;
    }
}
