<?php

namespace AppBundle\Entity;

//use DateTime;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * 
 * OnCuadromo
 *
 * @ORM\Table(name="oncuadromo")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\OnCuadromoRepository")
 */
class OnCuadromo {

     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     ** @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var \AppBundle\Entity\OnPerdiodos
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\OnPeriodos")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="ON_PERIODOS_ID", referencedColumnName="id")
     * })
     */
    private $onperiodos;
    
    /**
     * @var string
     *
     * @ORM\Column(name="a", type="string", nullable=true)
     */
    private $a;
        
    /**
     * @var string
     *
     * @ORM\Column(name="b", type="string", nullable=true)
     */
    private $b;
    
    /**
     * @var string
     *
     * @ORM\Column(name="c", type="string", nullable=true)
     */
    private $c;
    
    /**
     * @var string
     *
     * @ORM\Column(name="d", type="string", nullable=true)
     */
    private $d;
    /**
     * @var string
     *
     * @ORM\Column(name="e", type="string", nullable=true)
     */
    private $e;
    /**
     * @var string
     *
     * @ORM\Column(name="f", type="string", nullable=true)
     */
    private $f;
    
    /**
     * @var string
     *
     * @ORM\Column(name="g", type="string", nullable=true)
     */
    private $g;
    
    /**
     * @var string
     *
     * @ORM\Column(name="h", type="string", nullable=true)
     */
    private $h;
    
    /**
     * @var string
     *
     * @ORM\Column(name="i", type="string", nullable=true)
     */
    private $i;
    
    /**
     * @var string
     *
     * @ORM\Column(name="j", type="string", nullable=true)
     */
    private $j;
    
    /**
     * @var string
     *
     * @ORM\Column(name="k", type="string", nullable=true)
     */
    private $k;
    
    /**
     * @var string
     *
     * @ORM\Column(name="l", type="string", nullable=true)
     */
    private $l;
    
    /**
     * @var string
     *
     * @ORM\Column(name="m", type="string", nullable=true)
     */
    private $m;
    
    /**
     * @var string
     *
     * @ORM\Column(name="n", type="string", nullable=true)
     */
    private $n;
    
    /**
     * @var string
     *
     * @ORM\Column(name="o", type="string", nullable=true)
     */
    private $o;
    
    /**
     * @var string
     *
     * @ORM\Column(name="p", type="string", nullable=true)
     */
    private $p;
    
    /**
     * @var string
     *
     * @ORM\Column(name="q", type="string", nullable=true)
     */
    private $q;
    
    /**
     * @var string
     *
     * @ORM\Column(name="r", type="string", nullable=true)
     */
    private $r;
    
    /**
     * @var string
     *
     * @ORM\Column(name="s", type="string", nullable=true)
     */
    private $s;
    
    /**
     * @var string
     *
     * @ORM\Column(name="t", type="string", nullable=true)
     */
    private $t;
    
    /**
     * @var string
     *
     * @ORM\Column(name="u", type="string", nullable=true)
     */
    private $u;
    
    /**
     * @var string
     *
     * @ORM\Column(name="v", type="string", nullable=true)
     */
    private $v;
    
    /**
     * @var string
     *
     * @ORM\Column(name="w", type="string", nullable=true)
     */
    private $w;
    
    /**
     * @var string
     *
     * @ORM\Column(name="x", type="string", nullable=true)
     */
    private $x;
    
    /**
     * @var string
     *
     * @ORM\Column(name="y", type="string", nullable=true)
     */
    private $y;
    
    /**
     * @var string
     *
     * @ORM\Column(name="z", type="string", nullable=true)
     */
    private $z;
    
    /**
     * @var string
     *
     * @ORM\Column(name="aa", type="string", nullable=true)
     */
    private $aa;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ab", type="string", nullable=true)
     */
    private $ab;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ac", type="string", nullable=true)
     */
    private $ac;
    
    
     public function __toString()
    {
      return $this->onperiodos->get();        
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

   
    /**
     * Set b
     *
     * @param string $b
     *
     * @return OnCuadromo
     */
    public function setB($b)
    {
        $this->b = $b;

        return $this;
    }

    /**
     * Get b
     *
     * @return string
     */
    public function getB()
    {
        return $this->b;
    }

    /**
     * Set c
     *
     * @param string $c
     *
     * @return OnCuadromo
     */
    public function setC($c)
    {
        $this->c = $c;

        return $this;
    }

    /**
     * Get c
     *
     * @return string
     */
    public function getC()
    {
        return $this->c;
    }

    /**
     * Set d
     *
     * @param string $d
     *
     * @return OnCuadromo
     */
    public function setD($d)
    {
        $this->d = $d;

        return $this;
    }

    /**
     * Get d
     *
     * @return string
     */
    public function getD()
    {
        return $this->d;
    }

    /**
     * Set e
     *
     * @param string $e
     *
     * @return OnCuadromo
     */
    public function setE($e)
    {
        $this->e = $e;

        return $this;
    }

    /**
     * Get e
     *
     * @return string
     */
    public function getE()
    {
        return $this->e;
    }

    /**
     * Set f
     *
     * @param string $f
     *
     * @return OnCuadromo
     */
    public function setF($f)
    {
        $this->f = $f;

        return $this;
    }

    /**
     * Get f
     *
     * @return string
     */
    public function getF()
    {
        return $this->f;
    }

    /**
     * Set g
     *
     * @param string $g
     *
     * @return OnCuadromo
     */
    public function setG($g)
    {
        $this->g = $g;

        return $this;
    }

    /**
     * Get g
     *
     * @return string
     */
    public function getG()
    {
        return $this->g;
    }

    /**
     * Set h
     *
     * @param string $h
     *
     * @return OnCuadromo
     */
    public function setH($h)
    {
        $this->h = $h;

        return $this;
    }

    /**
     * Get h
     *
     * @return string
     */
    public function getH()
    {
        return $this->h;
    }

    /**
     * Set i
     *
     * @param string $i
     *
     * @return OnCuadromo
     */
    public function setI($i)
    {
        $this->i = $i;

        return $this;
    }

    /**
     * Get i
     *
     * @return string
     */
    public function getI()
    {
        return $this->i;
    }

    /**
     * Set j
     *
     * @param string $j
     *
     * @return OnCuadromo
     */
    public function setJ($j)
    {
        $this->j = $j;

        return $this;
    }

    /**
     * Get j
     *
     * @return string
     */
    public function getJ()
    {
        return $this->j;
    }

    /**
     * Set k
     *
     * @param string $k
     *
     * @return OnCuadromo
     */
    public function setK($k)
    {
        $this->k = $k;

        return $this;
    }

    /**
     * Get k
     *
     * @return string
     */
    public function getK()
    {
        return $this->k;
    }

    /**
     * Set l
     *
     * @param string $l
     *
     * @return OnCuadromo
     */
    public function setL($l)
    {
        $this->l = $l;

        return $this;
    }

    /**
     * Get l
     *
     * @return string
     */
    public function getL()
    {
        return $this->l;
    }

    /**
     * Set m
     *
     * @param string $m
     *
     * @return OnCuadromo
     */
    public function setM($m)
    {
        $this->m = $m;

        return $this;
    }

    /**
     * Get m
     *
     * @return string
     */
    public function getM()
    {
        return $this->m;
    }

    /**
     * Set n
     *
     * @param string $n
     *
     * @return OnCuadromo
     */
    public function setN($n)
    {
        $this->n = $n;

        return $this;
    }

    /**
     * Get n
     *
     * @return string
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * Set o
     *
     * @param string $o
     *
     * @return OnCuadromo
     */
    public function setO($o)
    {
        $this->o = $o;

        return $this;
    }

    /**
     * Get o
     *
     * @return string
     */
    public function getO()
    {
        return $this->o;
    }

    /**
     * Set p
     *
     * @param string $p
     *
     * @return OnCuadromo
     */
    public function setP($p)
    {
        $this->p = $p;

        return $this;
    }

    /**
     * Get p
     *
     * @return string
     */
    public function getP()
    {
        return $this->p;
    }

    /**
     * Set q
     *
     * @param string $q
     *
     * @return OnCuadromo
     */
    public function setQ($q)
    {
        $this->q = $q;

        return $this;
    }

    /**
     * Get q
     *
     * @return string
     */
    public function getQ()
    {
        return $this->q;
    }

    /**
     * Set r
     *
     * @param string $r
     *
     * @return OnCuadromo
     */
    public function setR($r)
    {
        $this->r = $r;

        return $this;
    }

    /**
     * Get r
     *
     * @return string
     */
    public function getR()
    {
        return $this->r;
    }

    /**
     * Set s
     *
     * @param string $s
     *
     * @return OnCuadromo
     */
    public function setS($s)
    {
        $this->s = $s;

        return $this;
    }

    /**
     * Get s
     *
     * @return string
     */
    public function getS()
    {
        return $this->s;
    }

    /**
     * Set t
     *
     * @param string $t
     *
     * @return OnCuadromo
     */
    public function setT($t)
    {
        $this->t = $t;

        return $this;
    }

    /**
     * Get t
     *
     * @return string
     */
    public function getT()
    {
        return $this->t;
    }

    /**
     * Set u
     *
     * @param string $u
     *
     * @return OnCuadromo
     */
    public function setU($u)
    {
        $this->u = $u;

        return $this;
    }

    /**
     * Get u
     *
     * @return string
     */
    public function getU()
    {
        return $this->u;
    }

    /**
     * Set v
     *
     * @param string $v
     *
     * @return OnCuadromo
     */
    public function setV($v)
    {
        $this->v = $v;

        return $this;
    }

    /**
     * Get v
     *
     * @return string
     */
    public function getV()
    {
        return $this->v;
    }

    /**
     * Set w
     *
     * @param string $w
     *
     * @return OnCuadromo
     */
    public function setW($w)
    {
        $this->w = $w;

        return $this;
    }

    /**
     * Get w
     *
     * @return string
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * Set x
     *
     * @param string $x
     *
     * @return OnCuadromo
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return string
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param string $y
     *
     * @return OnCuadromo
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return string
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set z
     *
     * @param string $z
     *
     * @return OnCuadromo
     */
    public function setZ($z)
    {
        $this->z = $z;

        return $this;
    }

    /**
     * Get z
     *
     * @return string
     */
    public function getZ()
    {
        return $this->z;
    }

    /**
     * Set aa
     *
     * @param string $aa
     *
     * @return OnCuadromo
     */
    public function setAa($aa)
    {
        $this->aa = $aa;

        return $this;
    }

    /**
     * Get aa
     *
     * @return string
     */
    public function getAa()
    {
        return $this->aa;
    }

    /**
     * Set ab
     *
     * @param string $ab
     *
     * @return OnCuadromo
     */
    public function setAb($ab)
    {
        $this->ab = $ab;

        return $this;
    }

    /**
     * Get ab
     *
     * @return string
     */
    public function getAb()
    {
        return $this->ab;
    }

    /**
     * Set ac
     *
     * @param string $ac
     *
     * @return OnCuadromo
     */
    public function setAc($ac)
    {
        $this->ac = $ac;

        return $this;
    }

    /**
     * Get ac
     *
     * @return string
     */
    public function getAc()
    {
        return $this->ac;
    }

    /**
     * Set onperiodos
     *
     * @param \AppBundle\Entity\OnPeriodos $onperiodos
     *
     * @return OnCuadromo
     */
    public function setOnperiodos(\AppBundle\Entity\OnPeriodos $onperiodos = null)
    {
        $this->onperiodos = $onperiodos;

        return $this;
    }

    /**
     * Get onperiodos
     *
     * @return \AppBundle\Entity\OnPeriodos
     */
    public function getOnperiodos()
    {
        return $this->onperiodos;
    }

    /**
     * Set a
     *
     * @param string $a
     *
     * @return OnCuadromo
     */
    public function setA($a)
    {
        $this->a = $a;

        return $this;
    }

    /**
     * Get a
     *
     * @return string
     */
    public function getA()
    {
        return $this->a;
    }
}
