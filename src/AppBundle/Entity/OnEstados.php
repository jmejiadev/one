<?php

namespace AppBundle\Entity;

//use DateTime;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * 
 * OnEstados
 *
 * @ORM\Table(name="onestados")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\OnEstadosRepository")
 */
class OnEstados {

     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     ** @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
       
    /**
     * @var string
     *
     * @ORM\Column(name="EST_CODIGO", type="string", nullable=true)
     */
    private $estCodigo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="EST_NOMBRE", type="string", nullable=true)
     */
    private $estNombre;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="EST_TIEMPO", type="integer", nullable=true)
     */
    private $estTiempo;
    
     public function __toString()
    {
        return $this->estCodigo();        
    }
    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estCodigo
     *
     * @param string $estCodigo
     *
     * @return OnEstados
     */
    public function setEstCodigo($estCodigo)
    {
        $this->estCodigo = $estCodigo;

        return $this;
    }

    /**
     * Get estCodigo
     *
     * @return string
     */
    public function getEstCodigo()
    {
        return $this->estCodigo;
    }

    /**
     * Set estNombre
     *
     * @param string $estNombre
     *
     * @return OnEstados
     */
    public function setEstNombre($estNombre)
    {
        $this->estNombre = $estNombre;

        return $this;
    }

    /**
     * Get estNombre
     *
     * @return string
     */
    public function getEstNombre()
    {
        return $this->estNombre;
    }

    /**
     * Set estTiempo
     *
     * @param integer $estTiempo
     *
     * @return OnEstados
     */
    public function setEstTiempo($estTiempo)
    {
        $this->estTiempo = $estTiempo;

        return $this;
    }

    /**
     * Get estTiempo
     *
     * @return integer
     */
    public function getEstTiempo()
    {
        return $this->estTiempo;
    }
}
