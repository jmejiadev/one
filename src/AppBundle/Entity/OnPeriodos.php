<?php

namespace AppBundle\Entity;

//use DateTime;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * 
 * OnPeriodos
 *
 * @ORM\Table(name="onperiodos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\OnPeriodosRepository")
 */
class OnPeriodos {

     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     ** @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var \AppBundle\Entity\OnCampanas
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\OnCampanas")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="ON_CAMPANAS_ID", referencedColumnName="id")
     * })
     */
    private $oncampanas;
    
    /**
     * @var string
     *
     * @ORM\Column(name="PER_NOMBRE", type="string", nullable=true)
     */
    private $perNombre;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="PER_FECHA", type="datetime", nullable=false)
     */
    private $perFecha;
    
    /**
     * @var string
     *
     * @ORM\Column(name="PER_EST_PRO", type="string", nullable=true)
     */
    private $perEstPro;
    
    /**
     * @var string
     *
     * @ORM\Column(name="PER_EST_TIE", type="string", nullable=true)
     */
    private $perEstTie;
    
    /**
     * @var string
     *
     * @ORM\Column(name="PER_ESTADO", type="string", nullable=true)
     */
    private $perEstado;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="PER_OBSERVACION", type="string", nullable=true)
     */
    private $perObservacion;
    
     public function __toString()
    {
        return $this->perNombre();        
    }
    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set perNombre
     *
     * @param string $perNombre
     *
     * @return OnPeriodos
     */
    public function setPerNombre($perNombre)
    {
        $this->perNombre = $perNombre;

        return $this;
    }

    /**
     * Get perNombre
     *
     * @return string
     */
    public function getPerNombre()
    {
        return $this->perNombre;
    }

    /**
     * Set perFecha
     *
     * @param \DateTime $perFecha
     *
     * @return OnPeriodos
     */
    public function setPerFecha($perFecha)
    {
        $this->perFecha = $perFecha;

        return $this;
    }

    /**
     * Get perFecha
     *
     * @return \DateTime
     */
    public function getPerFecha()
    {
        return $this->perFecha;
    }

    /**
     * Set perEstado
     *
     * @param string $perEstado
     *
     * @return OnPeriodos
     */
    public function setPerEstado($perEstado)
    {
        $this->perEstado = $perEstado;

        return $this;
    }

    /**
     * Get perEstado
     *
     * @return string
     */
    public function getPerEstado()
    {
        return $this->perEstado;
    }

    /**
     * Set perObservacion
     *
     * @param string $perObservacion
     *
     * @return OnPeriodos
     */
    public function setPerObservacion($perObservacion)
    {
        $this->perObservacion = $perObservacion;

        return $this;
    }

    /**
     * Get perObservacion
     *
     * @return string
     */
    public function getPerObservacion()
    {
        return $this->perObservacion;
    }

    /**
     * Set perEstPro
     *
     * @param string $perEstPro
     *
     * @return OnPeriodos
     */
    public function setPerEstPro($perEstPro)
    {
        $this->perEstPro = $perEstPro;

        return $this;
    }

    /**
     * Get perEstPro
     *
     * @return string
     */
    public function getPerEstPro()
    {
        return $this->perEstPro;
    }

    /**
     * Set perEstTie
     *
     * @param string $perEstTie
     *
     * @return OnPeriodos
     */
    public function setPerEstTie($perEstTie)
    {
        $this->perEstTie = $perEstTie;

        return $this;
    }

    /**
     * Get perEstTie
     *
     * @return string
     */
    public function getPerEstTie()
    {
        return $this->perEstTie;
    }

    /**
     * Set oncampanas
     *
     * @param \AppBundle\Entity\OnCampanas $oncampanas
     *
     * @return OnPeriodos
     */
    public function setOncampanas(\AppBundle\Entity\OnCampanas $oncampanas = null)
    {
        $this->oncampanas = $oncampanas;

        return $this;
    }

    /**
     * Get oncampanas
     *
     * @return \AppBundle\Entity\OnCampanas
     */
    public function getOncampanas()
    {
        return $this->oncampanas;
    }
}
