<?php

namespace AppBundle\Entity;

//use DateTime;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * OnProductividad
 *
 * @ORM\Table(name="onproductividad")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\OnProductividadRepository")
 */
class OnProductividad {

     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     ** @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var \AppBundle\Entity\OnCargas
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\OnCargas")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="ON_CARGAS_ID", referencedColumnName="id")
     * })
     */
    private $oncargas;
    
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;
    
    /**
     * @var string
     *
     * @ORM\Column(name="siteid", type="string", nullable=true)
     */
    private $siteid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="subsiteid", type="string", nullable=true)
     */
    private $subsiteid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="i3user", type="string", nullable=true)
     */
    private $i3user;
    
    /**
     * @var string
     *
     * @ORM\Column(name="queue", type="string", nullable=true)
     */
    private $queue;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totEnteredACD", type="string", nullable=true)
     */
    private $totEnteredACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totnAnswered", type="string", nullable=true)
     */
    private $totnAnswered;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totnAnsweredACD", type="string", nullable=true)
     */
    private $totnAnsweredACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totnAbandonedACD", type="string", nullable=true)
     */
    private $totnAbandonedACD;
    /**
     * @var string
     *
     * @ORM\Column(name="totnFlowOutAcd", type="string", nullable=true)
     */
    private $totnFlowOutAcd;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totnTransferedACD", type="string", nullable=true)
     */
    private $totnTransferedACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totnIntToIntACD", type="string", nullable=true)
     */
    private $totnIntToIntACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totnExtToInt", type="string", nullable=true)
     */
    private $totnExtToInt;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totnIntToExt", type="string", nullable=true)
     */
    private $totnIntToExt;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totTTalkACD", type="string", nullable=true)
     */
    private $totTTalkACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totTACW", type="string", nullable=true)
     */
    private $totTACW;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totTHoldACD", type="string", nullable=true)
     */
    private $totTHoldACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totTAgentLoggedin", type="string", nullable=true)
     */
    private $totTAgentLoggedin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totTAgentLoggedin2", type="string", nullable=true)
     */
    private $totTAgentLoggedin2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totTAgentAvailable", type="string", nullable=true)
     */
    private $totTAgentAvailable;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totTAgentACDLoggedIn", type="string", nullable=true)
     */
    private $totTAgentACDLoggedIn;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totnExttoIntNONACD", type="string", nullable=true)
     */
    private $totnExttoIntNONACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totnInttoExtNONACD", type="string", nullable=true)
     */
    private $totnInttoExtNONACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totTExttoIntNONACD", type="string", nullable=true)
     */
    private $totTExttoIntNONACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="totTInttoExtNONACD", type="string", nullable=true)
     */
    private $totTInttoExtNONACD;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Active", type="string", nullable=true)
     */
    private $Active;
    
    /**
     * @var string
     *
     * @ORM\Column(name="IsPrivate", type="string", nullable=true)
     */
    private $IsPrivate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="IndivTypeID", type="string", nullable=true)
     */
    private $IndivTypeID;
    
    /**
     * @var string
     *
     * @ORM\Column(name="TitleID", type="string", nullable=true)
     */
    private $TitleID;
    
    /**
     * @var string
     *
     * @ORM\Column(name="IndivID", type="string", nullable=true)
     */
    private $IndivID;
    
    /**
     * @var string
     *
     * @ORM\Column(name="LastName", type="string", nullable=true)
     */
    private $LastName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="FirstName", type="string", nullable=true)
     */
    private $FirstName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="MiddleName", type="string", nullable=true)
     */
    private $MiddleName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="DisplayUserName", type="string", nullable=true)
     */
    private $DisplayUserName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="JobTitle", type="string", nullable=true)
     */
    private $JobTitle;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ICUserID", type="string", nullable=true)
     */
    private $ICUserID;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ExtID", type="string", nullable=true)
     */
    private $ExtID;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ExtSource", type="string", nullable=true)
     */
    private $ExtSource;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="rank", type="string", nullable=true)
     */
    private $rank;
    
    /**
     * @var string
     *
     * @ORM\Column(name="PRO_OBSERVACIONES", type="string", nullable=true)
     */
    private $proObservaciones;
    
    
    public function __toString()
    {
        return $this->i3user;        
    }

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set siteid
     *
     * @param string $siteid
     *
     * @return OnProductividad
     */
    public function setSiteid($siteid)
    {
        $this->siteid = $siteid;

        return $this;
    }

    /**
     * Get siteid
     *
     * @return string
     */
    public function getSiteid()
    {
        return $this->siteid;
    }

    /**
     * Set subsiteid
     *
     * @param string $subsiteid
     *
     * @return OnProductividad
     */
    public function setSubsiteid($subsiteid)
    {
        $this->subsiteid = $subsiteid;

        return $this;
    }

    /**
     * Get subsiteid
     *
     * @return string
     */
    public function getSubsiteid()
    {
        return $this->subsiteid;
    }

    /**
     * Set i3user
     *
     * @param string $i3user
     *
     * @return OnProductividad
     */
    public function setI3user($i3user)
    {
        $this->i3user = $i3user;

        return $this;
    }

    /**
     * Get i3user
     *
     * @return string
     */
    public function getI3user()
    {
        return $this->i3user;
    }

    /**
     * Set queue
     *
     * @param string $queue
     *
     * @return OnProductividad
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;

        return $this;
    }

    /**
     * Get queue
     *
     * @return string
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * Set totEnteredACD
     *
     * @param string $totEnteredACD
     *
     * @return OnProductividad
     */
    public function setTotEnteredACD($totEnteredACD)
    {
        $this->totEnteredACD = $totEnteredACD;

        return $this;
    }

    /**
     * Get totEnteredACD
     *
     * @return string
     */
    public function getTotEnteredACD()
    {
        return $this->totEnteredACD;
    }

    /**
     * Set totnAnswered
     *
     * @param string $totnAnswered
     *
     * @return OnProductividad
     */
    public function setTotnAnswered($totnAnswered)
    {
        $this->totnAnswered = $totnAnswered;

        return $this;
    }

    /**
     * Get totnAnswered
     *
     * @return string
     */
    public function getTotnAnswered()
    {
        return $this->totnAnswered;
    }

    /**
     * Set totnAnsweredACD
     *
     * @param string $totnAnsweredACD
     *
     * @return OnProductividad
     */
    public function setTotnAnsweredACD($totnAnsweredACD)
    {
        $this->totnAnsweredACD = $totnAnsweredACD;

        return $this;
    }

    /**
     * Get totnAnsweredACD
     *
     * @return string
     */
    public function getTotnAnsweredACD()
    {
        return $this->totnAnsweredACD;
    }

    /**
     * Set totnAbandonedACD
     *
     * @param string $totnAbandonedACD
     *
     * @return OnProductividad
     */
    public function setTotnAbandonedACD($totnAbandonedACD)
    {
        $this->totnAbandonedACD = $totnAbandonedACD;

        return $this;
    }

    /**
     * Get totnAbandonedACD
     *
     * @return string
     */
    public function getTotnAbandonedACD()
    {
        return $this->totnAbandonedACD;
    }

    /**
     * Set totnFlowOutAcd
     *
     * @param string $totnFlowOutAcd
     *
     * @return OnProductividad
     */
    public function setTotnFlowOutAcd($totnFlowOutAcd)
    {
        $this->totnFlowOutAcd = $totnFlowOutAcd;

        return $this;
    }

    /**
     * Get totnFlowOutAcd
     *
     * @return string
     */
    public function getTotnFlowOutAcd()
    {
        return $this->totnFlowOutAcd;
    }

    /**
     * Set totnTransferedACD
     *
     * @param string $totnTransferedACD
     *
     * @return OnProductividad
     */
    public function setTotnTransferedACD($totnTransferedACD)
    {
        $this->totnTransferedACD = $totnTransferedACD;

        return $this;
    }

    /**
     * Get totnTransferedACD
     *
     * @return string
     */
    public function getTotnTransferedACD()
    {
        return $this->totnTransferedACD;
    }

    /**
     * Set totnIntToIntACD
     *
     * @param string $totnIntToIntACD
     *
     * @return OnProductividad
     */
    public function setTotnIntToIntACD($totnIntToIntACD)
    {
        $this->totnIntToIntACD = $totnIntToIntACD;

        return $this;
    }

    /**
     * Get totnIntToIntACD
     *
     * @return string
     */
    public function getTotnIntToIntACD()
    {
        return $this->totnIntToIntACD;
    }

    /**
     * Set totnExtToInt
     *
     * @param string $totnExtToInt
     *
     * @return OnProductividad
     */
    public function setTotnExtToInt($totnExtToInt)
    {
        $this->totnExtToInt = $totnExtToInt;

        return $this;
    }

    /**
     * Get totnExtToInt
     *
     * @return string
     */
    public function getTotnExtToInt()
    {
        return $this->totnExtToInt;
    }

    /**
     * Set totnIntToExt
     *
     * @param string $totnIntToExt
     *
     * @return OnProductividad
     */
    public function setTotnIntToExt($totnIntToExt)
    {
        $this->totnIntToExt = $totnIntToExt;

        return $this;
    }

    /**
     * Get totnIntToExt
     *
     * @return string
     */
    public function getTotnIntToExt()
    {
        return $this->totnIntToExt;
    }

    /**
     * Set totTTalkACD
     *
     * @param string $totTTalkACD
     *
     * @return OnProductividad
     */
    public function setTotTTalkACD($totTTalkACD)
    {
        $this->totTTalkACD = $totTTalkACD;

        return $this;
    }

    /**
     * Get totTTalkACD
     *
     * @return string
     */
    public function getTotTTalkACD()
    {
        return $this->totTTalkACD;
    }

    /**
     * Set totTACW
     *
     * @param string $totTACW
     *
     * @return OnProductividad
     */
    public function setTotTACW($totTACW)
    {
        $this->totTACW = $totTACW;

        return $this;
    }

    /**
     * Get totTACW
     *
     * @return string
     */
    public function getTotTACW()
    {
        return $this->totTACW;
    }

    /**
     * Set totTHoldACD
     *
     * @param string $totTHoldACD
     *
     * @return OnProductividad
     */
    public function setTotTHoldACD($totTHoldACD)
    {
        $this->totTHoldACD = $totTHoldACD;

        return $this;
    }

    /**
     * Get totTHoldACD
     *
     * @return string
     */
    public function getTotTHoldACD()
    {
        return $this->totTHoldACD;
    }

    /**
     * Set totTAgentLoggedin
     *
     * @param string $totTAgentLoggedin
     *
     * @return OnProductividad
     */
    public function setTotTAgentLoggedin($totTAgentLoggedin)
    {
        $this->totTAgentLoggedin = $totTAgentLoggedin;

        return $this;
    }

    /**
     * Get totTAgentLoggedin
     *
     * @return string
     */
    public function getTotTAgentLoggedin()
    {
        return $this->totTAgentLoggedin;
    }

    /**
     * Set totTAgentLoggedin2
     *
     * @param string $totTAgentLoggedin2
     *
     * @return OnProductividad
     */
    public function setTotTAgentLoggedin2($totTAgentLoggedin2)
    {
        $this->totTAgentLoggedin2 = $totTAgentLoggedin2;

        return $this;
    }

    /**
     * Get totTAgentLoggedin2
     *
     * @return string
     */
    public function getTotTAgentLoggedin2()
    {
        return $this->totTAgentLoggedin2;
    }

    /**
     * Set totTAgentAvailable
     *
     * @param string $totTAgentAvailable
     *
     * @return OnProductividad
     */
    public function setTotTAgentAvailable($totTAgentAvailable)
    {
        $this->totTAgentAvailable = $totTAgentAvailable;

        return $this;
    }

    /**
     * Get totTAgentAvailable
     *
     * @return string
     */
    public function getTotTAgentAvailable()
    {
        return $this->totTAgentAvailable;
    }

    /**
     * Set totTAgentACDLoggedIn
     *
     * @param string $totTAgentACDLoggedIn
     *
     * @return OnProductividad
     */
    public function setTotTAgentACDLoggedIn($totTAgentACDLoggedIn)
    {
        $this->totTAgentACDLoggedIn = $totTAgentACDLoggedIn;

        return $this;
    }

    /**
     * Get totTAgentACDLoggedIn
     *
     * @return string
     */
    public function getTotTAgentACDLoggedIn()
    {
        return $this->totTAgentACDLoggedIn;
    }

    /**
     * Set totnExttoIntNONACD
     *
     * @param string $totnExttoIntNONACD
     *
     * @return OnProductividad
     */
    public function setTotnExttoIntNONACD($totnExttoIntNONACD)
    {
        $this->totnExttoIntNONACD = $totnExttoIntNONACD;

        return $this;
    }

    /**
     * Get totnExttoIntNONACD
     *
     * @return string
     */
    public function getTotnExttoIntNONACD()
    {
        return $this->totnExttoIntNONACD;
    }

    /**
     * Set totnInttoExtNONACD
     *
     * @param string $totnInttoExtNONACD
     *
     * @return OnProductividad
     */
    public function setTotnInttoExtNONACD($totnInttoExtNONACD)
    {
        $this->totnInttoExtNONACD = $totnInttoExtNONACD;

        return $this;
    }

    /**
     * Get totnInttoExtNONACD
     *
     * @return string
     */
    public function getTotnInttoExtNONACD()
    {
        return $this->totnInttoExtNONACD;
    }

    /**
     * Set totTExttoIntNONACD
     *
     * @param string $totTExttoIntNONACD
     *
     * @return OnProductividad
     */
    public function setTotTExttoIntNONACD($totTExttoIntNONACD)
    {
        $this->totTExttoIntNONACD = $totTExttoIntNONACD;

        return $this;
    }

    /**
     * Get totTExttoIntNONACD
     *
     * @return string
     */
    public function getTotTExttoIntNONACD()
    {
        return $this->totTExttoIntNONACD;
    }

    /**
     * Set totTInttoExtNONACD
     *
     * @param string $totTInttoExtNONACD
     *
     * @return OnProductividad
     */
    public function setTotTInttoExtNONACD($totTInttoExtNONACD)
    {
        $this->totTInttoExtNONACD = $totTInttoExtNONACD;

        return $this;
    }

    /**
     * Get totTInttoExtNONACD
     *
     * @return string
     */
    public function getTotTInttoExtNONACD()
    {
        return $this->totTInttoExtNONACD;
    }

    /**
     * Set active
     *
     * @param string $active
     *
     * @return OnProductividad
     */
    public function setActive($active)
    {
        $this->Active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return string
     */
    public function getActive()
    {
        return $this->Active;
    }

    /**
     * Set isPrivate
     *
     * @param string $isPrivate
     *
     * @return OnProductividad
     */
    public function setIsPrivate($isPrivate)
    {
        $this->IsPrivate = $isPrivate;

        return $this;
    }

    /**
     * Get isPrivate
     *
     * @return string
     */
    public function getIsPrivate()
    {
        return $this->IsPrivate;
    }

    /**
     * Set indivTypeID
     *
     * @param string $indivTypeID
     *
     * @return OnProductividad
     */
    public function setIndivTypeID($indivTypeID)
    {
        $this->IndivTypeID = $indivTypeID;

        return $this;
    }

    /**
     * Get indivTypeID
     *
     * @return string
     */
    public function getIndivTypeID()
    {
        return $this->IndivTypeID;
    }

    /**
     * Set titleID
     *
     * @param string $titleID
     *
     * @return OnProductividad
     */
    public function setTitleID($titleID)
    {
        $this->TitleID = $titleID;

        return $this;
    }

    /**
     * Get titleID
     *
     * @return string
     */
    public function getTitleID()
    {
        return $this->TitleID;
    }

    /**
     * Set indivID
     *
     * @param string $indivID
     *
     * @return OnProductividad
     */
    public function setIndivID($indivID)
    {
        $this->IndivID = $indivID;

        return $this;
    }

    /**
     * Get indivID
     *
     * @return string
     */
    public function getIndivID()
    {
        return $this->IndivID;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return OnProductividad
     */
    public function setLastName($lastName)
    {
        $this->LastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->LastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return OnProductividad
     */
    public function setFirstName($firstName)
    {
        $this->FirstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->FirstName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     *
     * @return OnProductividad
     */
    public function setMiddleName($middleName)
    {
        $this->MiddleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->MiddleName;
    }

    /**
     * Set displayUserName
     *
     * @param string $displayUserName
     *
     * @return OnProductividad
     */
    public function setDisplayUserName($displayUserName)
    {
        $this->DisplayUserName = $displayUserName;

        return $this;
    }

    /**
     * Get displayUserName
     *
     * @return string
     */
    public function getDisplayUserName()
    {
        return $this->DisplayUserName;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     *
     * @return OnProductividad
     */
    public function setJobTitle($jobTitle)
    {
        $this->JobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string
     */
    public function getJobTitle()
    {
        return $this->JobTitle;
    }

    /**
     * Set iCUserID
     *
     * @param string $iCUserID
     *
     * @return OnProductividad
     */
    public function setICUserID($iCUserID)
    {
        $this->ICUserID = $iCUserID;

        return $this;
    }

    /**
     * Get iCUserID
     *
     * @return string
     */
    public function getICUserID()
    {
        return $this->ICUserID;
    }

    /**
     * Set extID
     *
     * @param string $extID
     *
     * @return OnProductividad
     */
    public function setExtID($extID)
    {
        $this->ExtID = $extID;

        return $this;
    }

    /**
     * Get extID
     *
     * @return string
     */
    public function getExtID()
    {
        return $this->ExtID;
    }

    /**
     * Set extSource
     *
     * @param string $extSource
     *
     * @return OnProductividad
     */
    public function setExtSource($extSource)
    {
        $this->ExtSource = $extSource;

        return $this;
    }

    /**
     * Get extSource
     *
     * @return string
     */
    public function getExtSource()
    {
        return $this->ExtSource;
    }

    /**
     * Set rank
     *
     * @param string $rank
     *
     * @return OnProductividad
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return string
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set proObservaciones
     *
     * @param string $proObservaciones
     *
     * @return OnProductividad
     */
    public function setProObservaciones($proObservaciones)
    {
        $this->proObservaciones = $proObservaciones;

        return $this;
    }

    /**
     * Get proObservaciones
     *
     * @return string
     */
    public function getProObservaciones()
    {
        return $this->proObservaciones;
    }

    /**
     * Set oncargas
     *
     * @param \AppBundle\Entity\OnCargas $oncargas
     *
     * @return OnProductividad
     */
    public function setOncargas(\AppBundle\Entity\OnCargas $oncargas = null)
    {
        $this->oncargas = $oncargas;

        return $this;
    }

    /**
     * Get oncargas
     *
     * @return \AppBundle\Entity\OnCargas
     */
    public function getOncargas()
    {
        return $this->oncargas;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return OnProductividad
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
