<?php

namespace AppBundle\Entity;

//use DateTime;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * 
 * OnTiempos
 *
 * @ORM\Table(name="ontiempos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\OnTiemposRepository")
 */
class OnTiempos {

     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     ** @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
       
    /**
     * @var \AppBundle\Entity\OnCargas
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\OnCargas")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="ON_CARGAS_ID", referencedColumnName="id")
     * })
     */
    private $oncargas;
    
     /**
     * @var string
     *
     * @ORM\Column(name="fecha", type="string", nullable=true)
     */
    private $fecha;
    
     /**
     * @var string
     *
     * @ORM\Column(name="i3user", type="string", nullable=true)
     */
    private $i3user;
    
     /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", nullable=true)
     */
    private $nombres;
    
     /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", nullable=true)
     */
    private $apellidos;
    
     /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", nullable=true)
     */
    private $estado;
    
     /**
     * @var string
     *
     * @ORM\Column(name="duracion", type="string", nullable=true)
     */
    private $duracion;
    
     /**
     * @var string
     *
     * @ORM\Column(name="estado2", type="string", nullable=true)
     */
    private $estado2;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="EST_TIEMPO", type="integer", nullable=true)
    */
    private $estTiempo;
    
     /**
     * @var string
     *
     * @ORM\Column(name="TIE_OBSERVACIONES", type="string", nullable=true)
     */
    private $tieObservaciones;
    
     public function __toString()
    {
        return $this->estado();        
    }
    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param string $fecha
     *
     * @return OnTiempos
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return string
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set i3user
     *
     * @param string $i3user
     *
     * @return OnTiempos
     */
    public function setI3user($i3user)
    {
        $this->i3user = $i3user;

        return $this;
    }

    /**
     * Get i3user
     *
     * @return string
     */
    public function getI3user()
    {
        return $this->i3user;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     *
     * @return OnTiempos
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return OnTiempos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return OnTiempos
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set duracion
     *
     * @param \DateTime $duracion
     *
     * @return OnTiempos
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return \DateTime
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set tieObservaciones
     *
     * @param string $tieObservaciones
     *
     * @return OnTiempos
     */
    public function setTieObservaciones($tieObservaciones)
    {
        $this->tieObservaciones = $tieObservaciones;

        return $this;
    }

    /**
     * Get tieObservaciones
     *
     * @return string
     */
    public function getTieObservaciones()
    {
        return $this->tieObservaciones;
    }

    /**
     * Set oncargas
     *
     * @param \AppBundle\Entity\OnCargas $oncargas
     *
     * @return OnTiempos
     */
    public function setOncargas(\AppBundle\Entity\OnCargas $oncargas = null)
    {
        $this->oncargas = $oncargas;

        return $this;
    }

    /**
     * Get oncargas
     *
     * @return \AppBundle\Entity\OnCargas
     */
    public function getOncargas()
    {
        return $this->oncargas;
    }

    /**
     * Set estado2
     *
     * @param string $estado2
     *
     * @return OnTiempos
     */
    public function setEstado2($estado2)
    {
        $this->estado2 = $estado2;

        return $this;
    }

    /**
     * Get estado2
     *
     * @return string
     */
    public function getEstado2()
    {
        return $this->estado2;
    }

    /**
     * Set estTiempo
     *
     * @param integer $estTiempo
     *
     * @return OnTiempos
     */
    public function setEstTiempo($estTiempo)
    {
        $this->estTiempo = $estTiempo;

        return $this;
    }

    /**
     * Get estTiempo
     *
     * @return integer
     */
    public function getEstTiempo()
    {
        return $this->estTiempo;
    }
}
