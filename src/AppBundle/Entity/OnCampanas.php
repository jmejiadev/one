<?php

namespace AppBundle\Entity;

//use DateTime;
use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * 
 * OnCampanas
 *
 * @ORM\Table(name="oncampanas")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\OnCampanasRepository")
 */
class OnCampanas {

     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     ** @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
       
    /**
     * @var string
     *
     * @ORM\Column(name="CAM_NOMBRE", type="string", nullable=true)
     */
    private $camNombre;
    
    /**
     * @var string
     *
     * @ORM\Column(name="CAM_ESTADO", type="string", nullable=true)
     */
    private $camEstado;
    
     public function __toString()
    {
        return $this->camNombre;        
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set camNombre
     *
     * @param string $camNombre
     *
     * @return OnCampanas
     */
    public function setCamNombre($camNombre)
    {
        $this->camNombre = $camNombre;

        return $this;
    }

    /**
     * Get camNombre
     *
     * @return string
     */
    public function getCamNombre()
    {
        return $this->camNombre;
    }

    /**
     * Set camEstado
     *
     * @param string $camEstado
     *
     * @return OnCampanas
     */
    public function setCamEstado($camEstado)
    {
        $this->camEstado = $camEstado;

        return $this;
    }

    /**
     * Get camEstado
     *
     * @return string
     */
    public function getCamEstado()
    {
        return $this->camEstado;
    }
}
