<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OnPeriodosType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('perNombre')->add('perFecha', 'datetime',array(
            'data'=> new \DateTime('now'),
            'format'=>'yyyy-MM-dd HH:mm:ss',
            'widget' => 'single_text',
            'read_only' => true,
        ))->add('perEstPro')->add('perEstTie')->add('perEstado')->add('perObservacion','textarea',array(
            'required'=>false
        ))->add('oncampanas');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\OnPeriodos'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_onperiodos';
    }


}
