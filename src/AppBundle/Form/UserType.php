<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(!$builder->getData()->getId()) {
            $builder->add('usuLider')->add('usuNomape')->add('username')->add('email')->add('enabled')->add('usuNombres')->add('usuApellidos')
                ->add('usuApenom')->add('usuCola')
                ->add('password', 'password', array(
                    'required' => false,
                    'attr' => array('autocomplete' => 'new-password')));
        }else{
            $builder->add('usuLider')->add('usuNomape')->add('username')->add('email')->add('enabled')->add('usuNombres')->add('usuApellidos')
                ->add('usuApenom')->add('usuCola');
        }
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
