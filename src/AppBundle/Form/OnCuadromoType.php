<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OnCuadromoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('a')->add('b')->add('c')->add('d')->add('e')->add('f')->add('g')->add('h')->add('i')->add('j')->add('k')->add('l')->add('m')->add('n')->add('o')->add('p')->add('q')->add('r')->add('s')->add('t')->add('u')->add('v')->add('w')->add('x')->add('y')->add('z')->add('aa')->add('ab')->add('ac')->add('onperiodos');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\OnCuadromo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_oncuadromo';
    }


}
