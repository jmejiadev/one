<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OnProductividadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fecha')->add('siteid')->add('subsiteid')->add('i3user')->add('queue')->add('totEnteredACD')->add('totnAnswered')->add('totnAnsweredACD')->add('totnAbandonedACD')->add('totnFlowOutAcd')->add('totnTransferedACD')->add('totnIntToIntACD')->add('totnExtToInt')->add('totnIntToExt')->add('totTTalkACD')->add('totTACW')->add('totTHoldACD')->add('totTAgentLoggedin')->add('totTAgentLoggedin2')->add('totTAgentAvailable')->add('totTAgentACDLoggedIn')->add('totnExttoIntNONACD')->add('totnInttoExtNONACD')->add('totTExttoIntNONACD')->add('totTInttoExtNONACD')->add('Active')->add('IsPrivate')->add('IndivTypeID')->add('TitleID')->add('IndivID')->add('LastName')->add('FirstName')->add('MiddleName')->add('DisplayUserName')->add('JobTitle')->add('ICUserID')->add('ExtID')->add('ExtSource')->add('rank')->add('proObservaciones')->add('oncargas');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\OnProductividad'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_onproductividad';
    }


}
